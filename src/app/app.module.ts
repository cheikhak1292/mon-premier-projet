import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MenuModule } from '@syncfusion/ej2-angular-navigations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import {NavbarComponent} from './navbar/navbar.component';
import { MatIconModule } from '@angular/material/icon';
import { AProposComponent } from './a-propos/a-propos.component';
import { NosServiceComponent } from './nos-service/nos-service.component';
import { NosPartenairesComponent } from './nos-partenaires/nos-partenaires.component';
import { ContactezNousComponent } from './contactez-nous/contactez-nous.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AProposComponent,
    NosServiceComponent,
    NosPartenairesComponent,
    ContactezNousComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MenuModule,
    FormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
