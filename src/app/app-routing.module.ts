import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from './app.component'; 
import {AProposComponent} from './a-propos/a-propos.component';
import {ContactezNousComponent} from './contactez-nous/contactez-nous.component';
import {NosPartenairesComponent} from './nos-partenaires/nos-partenaires.component';
import {NosServiceComponent} from './nos-service/nos-service.component';
const routes: Routes = [
 {path:'propos', component: AProposComponent},
 {path:'service', component: NosServiceComponent},
 {path:'partenaire', component: NosPartenairesComponent},
 {path:'contact', component: ContactezNousComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
